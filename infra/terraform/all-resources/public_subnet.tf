// resource "aws_subnet" "public" {
//   vpc_id                  = aws_vpc.main.id
//   for_each                = zipmap(data.aws_availability_zones.available.names, var.public_subnets)
//     availability_zone      = each.key
//     cidr_block            = each.value
//   map_public_ip_on_launch = true       
//   tags                    = var.common_tags
// }

resource "aws_subnet" "public1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_cidr_block1
  map_public_ip_on_launch = true             # Assigns Public IP for instance  
  availability_zone       = "${var.region}a" # Deploys subnet to AZ 1
  tags                    = var.common_tags
}

resource "aws_subnet" "public2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_cidr_block2
  map_public_ip_on_launch = true
  availability_zone       = "${var.region}b"
  tags                    = var.common_tags
}


resource "aws_subnet" "public3" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_cidr_block3
  map_public_ip_on_launch = true
  availability_zone       = "${var.region}c"
  tags                    = var.common_tags
}
