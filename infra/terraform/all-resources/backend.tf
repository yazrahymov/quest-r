terraform {
  backend "s3" {
    bucket = "rearc-quest-all"
    key    = "us-east-2/vpc.tfsate"
    region = "us-east-1"
  }
}