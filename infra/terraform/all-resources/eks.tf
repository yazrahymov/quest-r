resource "aws_eks_cluster" "rearceks" {
  name     = "example"
  role_arn = aws_iam_role.eksrole.arn

  vpc_config {
    subnet_ids = [aws_subnet.public1.id, aws_subnet.public2.id, aws_subnet.public3.id ]
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.example-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.example-AmazonEKSVPCResourceController,
  ]
}

output "endpoint" {
  value = aws_eks_cluster.rearceks.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.rearceks.certificate_authority[0].data
}



## node group

resource "aws_eks_node_group" "eksnodegroup" {
  cluster_name    = aws_eks_cluster.rearceks.name
  node_group_name = "rearc-eks-node-group"
  node_role_arn   = aws_iam_role.eksnodegrouprole.arn
  subnet_ids      = [aws_subnet.public1.id, aws_subnet.public2.id, aws_subnet.public3.id]

  scaling_config {
    desired_size = 2
    max_size     = 4
    min_size     = 2
  }

  update_config {
    max_unavailable = 2
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.example-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.example-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.example-AmazonEC2ContainerRegistryReadOnly,
  ]
}
data "tls_certificate" "example" {
  url = aws_eks_cluster.rearceks.identity[0].oidc[0].issuer
}

resource "aws_iam_openid_connect_provider" "example" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.example.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.rearceks.identity[0].oidc[0].issuer
}

data "aws_iam_policy_document" "example_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(aws_iam_openid_connect_provider.example.url, "https://", "")}:sub"
      values   = ["system:serviceaccount:kube-system:aws-node"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.example.arn]
      type        = "Federated"
    }
  }
}

resource "aws_iam_role" "example" {
  assume_role_policy = data.aws_iam_policy_document.example_assume_role_policy.json
  name               = "example"
}

resource "aws_iam_role" "s3-writer" {
  name                = "s3-writer"
  path                = "/"
  assume_role_policy  = data.aws_iam_policy_document.example_assume_role_policy.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonS3FullAccess"]
}



// provider "kubernetes" {
//   host                   = aws_eks_cluster.rearceks.endpoint
//   cluster_ca_certificate = base64decode(aws_eks_cluster.default.certificate_authority[0].data)
//   token                  = aws_eks_cluster_auth.rearceks.token
// }

// provider "helm" {
//   kubernetes {
//     host                   = data.aws_eks_cluster.rearceks.endpoint
//     cluster_ca_certificate = base64decode(aws_eks_cluster.rearceks.certificate_authority[0].data)
//     token                  = data.aws_eks_cluster_auth.rearceks.token
//   }
// }

// resource "local_file" "kubeconfig" {
//   sensitive_content = templatefile("${path.module}/kubeconfig.tpl", {
//     cluster_name = "example"
//     clusterca    = aws_eks_cluster.rearceks.certificate_authority[0].data,
//     endpoint     = aws_eks_cluster.rearceks.endpoint,
//     })
//   filename          = "./kubeconfig-example"
// }

// resource "kubernetes_namespace" "test" {
//   metadata {
//     name = "test"
//   }
// }

// resource "helm_release" "nginx_ingress" {
//   namespace  = kubernetes_namespace.test.metadata.0.name
//   wait       = true
//   timeout    = 600

//   name       = "ingress-nginx"

//   repository = "https://kubernetes.github.io/ingress-nginx"
//   chart      = "ingress-nginx"
//   version    = "v3.30.0"
// }


resource "kubernetes_service_account" "example" {
  metadata {
    name = "s3-writer"
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.s3-writer.arn
    }
  }
}