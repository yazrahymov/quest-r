variable "cidr_block" {
  default = "10.0.0.0/16" 
}

variable "region" {
  default = "us-east-2"
  type = string
}
// variable "public_subnets" {
//   type = list(string)
//   default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  
// }
// variable "private_subnets" {

// }

// variable "private_cidr_block1" {

// }
// variable "private_cidr_block2" {

// }
// variable "private_cidr_block3" {

// }
variable "public_cidr_block1" {

}
variable "public_cidr_block2" {

}
variable "public_cidr_block3" {

}

variable "common_tags" {
  type = map(string)
  description = "Tags"
}

variable "ecr_repo" {

}
variable "domain_name" {
  
} 