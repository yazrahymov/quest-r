# Rearc-Quest 

* Created [quest-r/quest/Dockerfile](Dockerfile) with Nodejs app requirements 
* Created infra/terraform folder and created s3 backend, dynamodb Lock Table, vpc with public subnets, nat gateway, internet gateway, route tables. Created `EKS` cluster with necessary IAM roles and attachments and added managed node group which is EC2 instances min number is 2, max number of EC2 is 4 and desired state also 2. Every resource has common tags which is in tfvars and defined in  `varibale.tf`. 
*  Terraform version is `0.14`
*  In the cluster folder created dns-rbac which is intended to use `external-dns` responsible to create external dns. Also has `external-dns.json` file is iam role policy which is added. 
*  Created `external-dns-service.yaml` responsible to create deployment --> replicaset --> pod. Deployment will be exposed to Load Balancer service type port 80 targetPort 3000 beacuse container port was exposed to port 3000
*  Every task was almost completed forgot to take screenshot to `loadblancer/secret_word` but I checked it and secret word is TwelveFactor. And `TLS` certificate was created in the terraform `acm.tf` file and created route53 record but did not issue beacuse takes time to issue. 
* ecr-push-commands.txt file is how I pushed the Docker image to ECR(Elastic Container Registry).


## Improvements

* infrastructure terraform files could be seperated and create company module and will be used all the time(VPC, EKS, ECS, ECR S3 etc ..) just reference to module or just get by `data` or if we have access to all `remote_state` we could also use it to get all info for our infratructure
* Configure route53 hosted zone and record
* Create Nginx Ingress controller which will be super easy to work with microservices
* yazrahymov.com --> Application LoadBalancer --> Nginx ingress controller --> Services --> Deployments --> Replicaset --> Pod --> container
* In the Ingress manifest file we could define the rule  host, path, services 
  ```
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: example-ingress
    annotations:
      ingress.kubernetes.io/rewrite-target: test.dev.rearc.com
  spec:
  rules:
  - http:
      paths:
        - path: /rearc-a
          backend:
            serviceName: rearc-a-service
            servicePort: 3000
        - path: /rearc-b
          backend:
            serviceName: rearc-b-service
            servicePort: 5000

* Install jenkins on different cluster or in the different namespace to automate build, deploy application. Create multibranch pipefile depending on github/bitbucket branches will scan(sonarqube etc) and build the image,  push or ecr/nexus. If build successful trigger the deploy job(pipeline) will deploy to env except prod(could be manual). Trigger build job we have to configure github/bitbucket webhook.
* All the application could be as helm charts.
* All the secret could be kept in the Vault. Use vault injector to inject api key or any other secrets by adding annotations to the deployment(k8s).
* Expose all the EKS cluster to the monitoring Datadog, Prometheus, Grafana
* Implement Service Mesh to secure deployment, pod namespace, service ingress/eggress communication. And Canary, Blue-Green deployment, enable the x-ray to trace microservices.

